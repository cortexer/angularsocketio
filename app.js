var express = require('express');

var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
app.set('port', process.env.PORT || 9000);
var server = require('http').Server(app);
var io = require('socket.io')(server);
var Moniker = require('moniker');
var restful = require('node-restful');
var mongoose = restful.mongoose;
var port = app.get('port');
var shortid = require('shortid');

app.use(express.static('public'));
app.use(cors());
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

mongoose.connect("mongodb://localhost/messages");

var MessageSchema = mongoose.Schema({
    username: String,
    message: String,
    date: Date,
    image: String,
    treeid: String
});

var Message = restful.model('message', MessageSchema);
Message.methods(['get', 'post', 'put', 'delete']);
Message.register(app, '/messages');

server.listen(port, function () {
    console.log("Server listening on: http://localhost:%s", port);
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

var usernames = {};
var rooms = [];

io.sockets.on('connection', function (socket) {
    
    socket.on('adduser', function (data) {
		console.log('adding user!');
		console.log(data);
        var username = data;
        if (username === 'Random name') {
            username = Moniker.choose();
			console.log('using Moniker!');
        } else {
            username = data;
			console.log('NOT using Moniker!');
        }
        var room = data.room;
        socket.username = username;
        socket.room = room;
        usernames[username] = username;
        socket.join(room);
        socket.emit('deletedbutton', username);
        socket.emit('updatechat', data);
        //socket.emit('updatechat', 'SERVER', 'You are connected. Start chatting');
        //socket.broadcast.to(room).emit('updatechat', 'SERVER', username + ' has connected to this room');
    });
    
    socket.on('createroom', function (data) {
		if (data) {
		var new_room = 1;
        rooms.push(new_room);
        data.room = new_room;
        socket.emit('roomcreated', data);
		}
    });
    
    socket.on('getPosts', function (messages) {
    var query = Message.find({});
    query.sort({date: 1}).limit(1000).exec(function (err, messages) {
        if (err) throw err;
		var messagesAmount = messages.length;
        socket.emit('updatechat', socket.username, messages, messagesAmount);
    })
    
    });

    socket.on('sendchat', function (messageObject) {
    message1 = messageObject.message;
    treeid = messageObject.treeid;
    var user = {};
    user.username = socket.username;
    user.message = message1;
    user.date = new Date().getTime();
    var colors = ['AliceBlue', 'Crimson', 'DarkMagenta ', 'Gold ', 'LightBlue ',
        'SteelBlue', 'red', 'LightGreen', 'blue', 'orange', 'yellow'];
    var color = colors[Math.floor(Math.random() * colors.length)];
    if (treeid === 'null') {
        user.treeid = shortid.generate() + '+' + color;
    } else {
        user.treeid = treeid;
    }
    console.log(user.message);
    console.log(user.treeid);
    user.image = 'http://dummyimage.com/250x250/000/fff&text=' + user.username.charAt(0).toUpperCase();
    new Message(user).save(function (err, room) {
        user._id = room._id;
		user.reply = 1;
        io.sockets.in(socket.room).emit('updatechat', socket.username, user);
    });    
    //socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', username + ' has connected to this room');
    //socket.emit('updatechat', 'SERVER', 'You are connected. Start chatting');
    });
    
    socket.on('deletePost', function (user) {
    Message.findById(user._id, function (err, doc) {
        doc.remove();
    })
    io.sockets.in(socket.room).emit('updateArray', user);
    });
    
    socket.on('editPost', function (user) {
    /*Message.update({_id: user._id}, { message: user.message },
    function(err, docs){ if(err) console.log(err); else console.log(docs); });*/
    Message.update({_id:user._id}, {message: user.message}, function(err, docs){ });
    io.sockets.in(socket.room).emit('updateArray', user);
    });
    


    socket.on('disconnect', function () {
        delete usernames[socket.username];
        io.sockets.emit('updateusers', usernames);
        if (socket.username !== undefined) {
            //socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
            socket.leave(socket.room);
        }
    });
    
});