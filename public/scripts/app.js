'use strict';

angular
.module('myApp', [])

/* Controllers */
.controller('AppCtrl', function ($scope, socket) {
  $scope.elementId;
  $scope.hideme = false;
  $scope.namesdiv = true;
  $scope.deleteConfirmation = 0;
  $scope.treeid = null;
  $scope.showMessage = 1;
  $scope.editForm = 0;
  $scope.deletebutton = false;
  $scope.currentUser = '';
  $scope.users = [];
  socket.on('connect', function () { $scope.users = []; });

  socket.on('updatechat', function (username, data, messagesAmount) {
    if (data) {
		if (data.reply) {
			$scope.users[$scope.users.length - 1].last = false;
			var user = {};
			user._id = data._id;
			user.username = data.username;
			user.message = data.message;
			user.date = data.date;
			user.image = data.image;
			user.treeid = data.treeid;
			user.treecolor = data.treeid.substring(data.treeid.indexOf("+") + 1);
			$scope.users.push(user);
			$scope.elementId = null;
			$scope.elementId = user._id;
		} else {
			data.forEach(function(data) {
				var user = {};
				user._id = data._id;
				user.username = data.username;
				user.message = data.message;
				user.date = data.date;
				user.image = data.image;
				user.treeid = data.treeid;
				user.treecolor = data.treeid.substring(data.treeid.indexOf("+") + 1);
				user.last = false;
				$scope.users.push(user);
			});
		}
			var amount = $scope.users.length - 1;
			$scope.users[amount].last = true;
	}
		
	setTimeout(function(){
	var element = document.getElementById('bottomDiv');
	element.scrollIntoView();
	}, 200);

  });

  socket.on('reloadposts', function (username) {
    $scope.currentUser = username;
    $scope.users = [];
    socket.emit('getPosts');
  });

  socket.on('roomcreated', function (data) {
    $scope.users = [];
    socket.emit('getPosts');
    socket.emit('adduser', data);
    $scope.namesdiv = false;
    $scope.chatbar = true;
	document.getElementById('chatbox').style.display = 'block';
	document.getElementById('chatbar').style.display = 'block';
  });

  socket.on('deletedbutton', function (username) {
	$scope.currentUser = username;
  });
  
  socket.on('updateArray', function (user) {
	$scope.users[user.index] = user;
  });
  
  $scope.changeTreeId = function (user) {
    
    if (!$scope.treeid) {
        $scope.treeid = user.treeid;
    } else {
        $scope.treeid = null;
    }
	
	setTimeout(function(){
	var element = document.getElementById('bottomDiv');
	if (element) {
		element.scrollIntoView();
	}	
	}, 200);
	
  }
 
  $scope.createRoom = function (data) {
    $scope.currentUser = data;
    socket.emit('createroom', data);
  }
  
  $scope.deleteMessage = function (user) {
    user.index = $scope.users.indexOf(user);
    var d = new Date();
    var n = d.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit', hour24: true}); 
    user.message = 'Message has been deleted at ' + n;
    user.deleted = 1;
    $scope.deleteConfirmation = 0;
    socket.emit('deletePost', user);
  }
  
  $scope.editMessage = function (message, user) {
    user.message = message;
    user.index = $scope.users.indexOf(user);
    $scope.editForm = 0;
    socket.emit('editPost', user);
  }

  $scope.doPost = function (message, treeid) {
	
	if (!treeid) {
		var treeid = 'null';
	}
	var messageObject = {
		message: message,
		treeid: treeid
	};
	$scope.message = '';
    socket.emit('sendchat', messageObject);
    $scope.deleteConfirmation = 0;
  }  
  
})

/* Services */
.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});